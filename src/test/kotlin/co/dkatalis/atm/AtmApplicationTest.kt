package co.dkatalis.atm

import io.mockk.every
import io.mockk.mockkStatic
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.*

internal class AtmApplicationTest {

    private val atm = AtmApplication()

    @Test
    fun `readInput() - Given input - Should capture input`() {
        // Given
        val expected = UUID.randomUUID().toString()

        mockkStatic("kotlin.io.ConsoleKt")
        every { readln() } returns expected

        // When
        val actual = atm.readInput()

        // Then
        assertEquals(expected, actual)
    }
}
